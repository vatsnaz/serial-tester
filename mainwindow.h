#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QStringList>
#include <QListWidgetItem>
#include <QtCore/QtGlobal>
#include <QtSerialPort/QSerialPort>
#include <QStringList>
#include <QObject>
#include <QTime>
#include <QTimer>

namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    enum {
        GET_FIRMWARE_VERSION = 0x01,
        SET_RTC = 0x02,
        GET_RTC = 0x03,
    };

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    static QByteArray getFirmwareVersion();

private slots:
    void handleReadyRead();
    void openSerialPort();
    void closeSerialPort();
    void on_startButton_clicked();
    void on_stopButton_clicked();
    void writeSerial();
    bool crcCheck(unsigned char *data, unsigned short size, unsigned short start, unsigned short crc);
    static unsigned short crcCalc(unsigned char *data, unsigned short size, unsigned short start);
    static unsigned short crcCalc(QByteArray data, unsigned short size, unsigned short start);
    void on_enableButton_clicked();
    void on_clearButton_clicked();

private:
    Ui::MainWindow *ui;
    QSerialPort *serial;
    QString portName;
    QTimer serialTimer;
    QTimer *txTimer;
    QByteArray readData;
    int c;
    int i;
    static QByteArray genericOutput(int cmd, int dataSize, QByteArray data);
    static QByteArray genericOutput(int cmd);
};

#endif // MAINWINDOW_H
