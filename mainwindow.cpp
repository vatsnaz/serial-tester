#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"
#include <QByteArray>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    portName = "COM2";
    serial = new QSerialPort;
    txTimer =new QTimer(this);
    serialTimer.setSingleShot(true);
    connect(serial, SIGNAL(readyRead()), SLOT(handleReadyRead()));
    connect(txTimer,SIGNAL(timeout()), this, SLOT(writeSerial()));
    i=0;
    c=0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_enableButton_clicked()
{
    if( ui->enableButton->text()=="Enable"){
        openSerialPort();
        ui->enableButton->setText("Disable");
    }else {
        closeSerialPort();
        ui->enableButton->setText("Enable");
    }
}

void MainWindow::on_startButton_clicked()
{
    txTimer->start(200);
}

void MainWindow::on_stopButton_clicked()
{
    if (txTimer->isActive() )
        txTimer->stop();
}

void MainWindow::on_clearButton_clicked()
{
    c=0;
    i=0;
    ui->Txlabel->setNum(0);
    ui->Rxlabel->setNum(0);
}

void MainWindow::openSerialPort()
{
    if (!serial) {
        serial = new QSerialPort;
    }

    serial->setPortName(portName);
    serial->setBaudRate(QSerialPort::Baud19200);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);

    if (serial->open(QIODevice::ReadWrite))
    {
     //   qDebug() << "open serial " << portName << " success";
        ui->statusBar->showMessage(tr("connect %1 success").arg(portName));
    }
    else
    {
      //  qDebug() << "open serial " << portName << " error";
        ui->statusBar->showMessage(tr("connect failed , code %1").arg(serial->error()));
    }

}

void MainWindow::closeSerialPort()
{
    if (serial) {
        serial->close();
        serial = NULL;
    }
    ui->statusBar->showMessage("disconnect");
}

QByteArray MainWindow::genericOutput(int cmd)
{
    QByteArray empty;
    return genericOutput(cmd, 0, empty);
}

QByteArray MainWindow::genericOutput(int cmd, int dataSize, QByteArray data)
{
    QByteArray newData;
    newData.append((char) 0xFF);
    newData.append((char) 0xEE);
    newData.append((char) dataSize);
    newData.append((char) cmd);

    if (dataSize > 0) {
        newData.append(data);
    }
    unsigned int crc = crcCalc(newData, newData.length(), 0);
    newData.append((crc >> 0) & 0xff);
    newData.append((crc >> 8) & 0xff);
    return newData;
}

QByteArray MainWindow::getFirmwareVersion()
{
    return genericOutput(MainWindow::GET_FIRMWARE_VERSION);
}

void MainWindow::writeSerial(){
    if (serial->portName() != portName) {
        serial->close();

        if (!serial->open(QIODevice::ReadWrite)) {
            qDebug()<<tr("Can't open %1, error code %2").arg(serial->portName()).arg(serial->error());
            return;
        }
    }
    qDebug() << "Message written";
    QByteArray msg = getFirmwareVersion();

    //write data
    serial->write(msg);

    //print send data
    for (unsigned char c :msg) {
        QString a,b;
        a = QString ("%1").arg((unsigned char)c, 0, 16).toUpper();
        b="0x"+a;
        qDebug() <<"send:" << b + " ";
    }
    serialTimer.start(50);
    c++;
    ui->Txlabel->setNum(c);
}

void MainWindow::handleReadyRead()
{
    QByteArray readData;
    //read data
    readData.append(serial->readAll());
    if (readData.isEmpty()) {
        qDebug() << tr("No data was currently available for reading from port %1").arg(portName);
    } else {
        qDebug() << tr("Data successfully received from port %1").arg(portName);

        //print read data
        for (unsigned char c : readData) {
            QString a,b;
            a = QString ("%1").arg((unsigned char)c, 0, 16).toUpper();
            b="0x"+a;
            qDebug() <<"read:" << b + " ";
        }
    }
    i++;
    ui->Rxlabel->setNum(i);
}


//crc
unsigned short MainWindow::crcCalc(QByteArray data, unsigned short size, unsigned short start)
{
    unsigned char *dst = (unsigned char *) data.data();

    return crcCalc(dst, size, start);
}

unsigned short MainWindow::crcCalc(unsigned char *data, unsigned short size, unsigned short start)
{
    unsigned short q, c, crcval;

    crcval = start;
    for (int i = 0; i < size; i++) {
        c = data[i] & 0xFF;
        q = (crcval ^ c) & 0x0F;
        crcval = (crcval >> 4) ^ (q * 0x1081);
        q = (crcval ^ (c >> 4)) & 0x0F;
        crcval = (crcval >> 4) ^ (q * 0x1081);
    }

    return crcval;
}

bool MainWindow::crcCheck(unsigned char *data, unsigned short size, unsigned short start, unsigned short crc)
{
    unsigned short crc2 = crcCalc(data, size, start);

    if (crc != crc2)
        return false;
    return true;
}
